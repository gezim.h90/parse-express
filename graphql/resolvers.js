var Parse = require('parse/node');
var GraphQLJSON = require('graphql-type-json');

Parse.initialize('myAppId', '456', '123');

Parse.serverURL = 'http://localhost:1337/parse';

// The root provides a resolver function for each API endpoint
const Resolvers = {
  allSchemas: () => {
    return Parse.Schema.all().then((value) => {
      return value;
    }).catch((err) => {
      console.log(err);
    });
  },
  schemaTypes: (arg) => {
    const schema = new Parse.Schema(arg.name);
    return schema.get().then((value) => {
      return value;
    }).catch((err) => {
      console.log(err);
    })
  },
  JSON: GraphQLJSON,
  addNewField: (args) => {
    const schema = new Parse.Schema(args.schemaName);

    schema.addField(args.fieldName, args.fieldType);

    return schema.update().then((res) => {
      return res;
    })
  },
  deleteField: (args) => {
    const schema = new Parse.Schema(args.schemaName);

    schema.deleteField(args.fieldName);
    return schema.update().then((res) => {
      console.log(res);
      return res;
    })
  },
};

module.exports = Resolvers;
