const GraphQLSchema = `
  scalar JSON
  type AllSchemas {
    className: String
  }
  type SchemaFields {
    fields: JSON
    classLevelPermissions: JSON
  }
  type Query {
    allSchemas: [AllSchemas],
    schemaTypes(name: String): SchemaFields
  },
  type Mutation {
    addNewField(schemaName: String, fieldName: String, fieldType: String): JSON
    deleteField(schemaName: String, fieldName: String): JSON
  }
`;

module.exports = GraphQLSchema;
